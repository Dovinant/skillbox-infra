# Ansible playbooks for Terraform

Ansible для работы с плейбуками этого каталога нужны:

Роли:
- [nahsi.cadvisor](https://galaxy.ansible.com/nahsi/cadvisor)
- [riemers.gitlab-runner](https://galaxy.ansible.com/riemers/gitlab-runner)

Коллекции:
- [community.general](https://galaxy.ansible.com/community/general)
- [ansible.windows](https://galaxy.ansible.com/ansible/windows)
- [ansible.posix](https://galaxy.ansible.com/ansible/posix)
- [community.docker](https://galaxy.ansible.com/community/docker)
- [community.grafana](https://galaxy.ansible.com/community/grafana)
- [prometheus.prometheus](https://galaxy.ansible.com/prometheus/prometheus)

Каталог содержит файлы:

- ansible.cfg - локальные настройка ansible
- ansible_install.yml - playbook для установки и запуска ansible на сервере runner
- blackbox.yml - playbook для установки и запуска blackbox exporter на сервере monitoring
- cadvisor.yml - playbook для установки и запуска cadvisor exporter на сервере web
- hosts - инвентарный файл (его создаёт terraform)
- docker_all_install.yml - playbook для установки и запуска docker на серверах web и runner
- gitlab-runner.yml - playbook для установки и регистрации gitlab-runner
- node_exporter.yml - playbook для установки и запуска node exporter на сервере web
- prometheus.yml - playbook для установки и запуска сервиса Prometheus на сервере monitoring
- main_grafana.yml - playbook для установки и запуска Grafana на сервере grafana, а также для подключения его к источнику данных Prometheus и экспорта на него нескольких dashboards. Этот плейбук запускает Terraform.
- main_prometheus.yml - объединяет плейбуки blackbox.yml и prometheus.yml. Этот плейбук запускает Terraform.
- main_runner.yml - объединяет плейбуки docker_all_install.yml, ansible_install.yml, gitlab-runner.yml. Этот плейбук запускает Terraform.
- main_web.yml - объединяет плейбуки docker_install.yml, node_exporter.yml, cadvisor.yml. Этот плейбук запускает Terraform.

Вложенный каталог group_vars содержит файл переменных:

- all.yml - в нём определены ansible_ssh_private_key_file и ansible_user

Вложенный каталог vars содержит файлы переменных:
- cadvisor_vars.yml – определены версия cadvisor и его контрольная сумма. [Подробнее смотри Read Me](https://galaxy.ansible.com/nahsi/cadvisor)
- grafana_vars.yml – определены порт и протокол для сервера grafana, пользователь и пароль для доступа к серверу, путь к локальному каталогу с dashboards в формате с json, источник данных для окружения prod, источник данных для окружения dev, ссылка на deb-file grafana на Яндекс диске.
- prometheus_vars.yml – определены время хранения данных на сервере Prometheus и максимальный размер этих данных.
- runner_vars.yml - в нём определены переменные для работы роли riemers.gitlab-runner. [Подробнее смотри Read Me](https://galaxy.ansible.com/riemers/gitlab-runner)

Вложенный каталог files содержит файлы:
- blackbox_exporter.yml – файл настройки модулей для blackbox_exporter
- daemon.json - файл настройки default logging driver Подробнее
- grafana_download.j2 – шаблон скрипта на Python. Этот скрипт скопирует deb-file grafana с Яндекс диска
- prometheus.yml.etc – файл настройки сервиса Prometheus

Вложенный каталог prometheus/targets содержит файл node.yml. В нём определены цели для Prometheus – откуда он собирает статистику.

Плейбуки этого каталога настраивают четыре сервера: grafana, monitoring, runner, web01.

# Сервер grafana
Сервер grafana устанавливает и настраивает плейбук main_grafana.yml. Напрямую установка Grafana из России невозможна, поэтому установочный файл загружен на Яндекс диск. Однако, ссылка с Яндекс диска  непригодна для скачивания файла, поэтому сначала генерируется скрипт на Python на основе grafana_download.j2 (фактически это уже готовый скрипт, в него только подставляется ссылка с Яндекс диска). Затем этот скрипт выполняется на целевом сервере и затем Grafana устанавливается из скачанного файла.

Далее плейбук убеждается что сервер grafana доступен и с помощью коллекции community.grafana.grafana_datasource настраивает источники данных для окружения prod и dev. Потом с помощью коллекции community.grafana.grafana_datasource плейбук устанавливает dashboards из локального каталога. В них уже прописана переменная, по которой можно выбирать из какого окружения Grafana показывает статистику.

Для работы плейбуку нужны несколько переменных. Переменные grafana_port, grafana_protocol, grafana_user, grafana_password, grafana_ds_path, prod_data_source, dev_data_source и deb_file_url определены в файле vars/grafana_vars.yml. Переменную grafana_ip передаёт Terraform - см. Terraform/servers.tf блок resource "yandex_compute_instance" "vm-4".

# Сервер monitoring
Чтобы настроить сервер monitoring Terraform запускает плейбук main_prometheus.yml, который объединяет плейбуки blackbox.yml и prometheus.yml.
- blackbox.yml запускает роль prometheus.prometheus.blackbox_exporter и указывает файл конфигурации для blackbox.
- prometheus.yml запускает роль prometheus.prometheus.prometheus и копирует файл конфигурации Prometheus.

# Сервер runner
Чтобы настроить сервер runner Terraform запускает плейбук main_runner.yml, который объединяет плейбуки docker_all_install.yml, ansible_install.yml, gitlab-runner.yml.
- docker_all_install.yml устанавливает сервис Docker.
- ansible_install.yml устанавливает ansible.
- gitlab-runner.yml с помощью роли riemers.gitlab-runner устанавливает gitlab-runner и для репозитория skillbox-diploma регистрирует два runners:

Example Docker GitLab Runner - в качестве executor использует Docker

Example Shell GitLab Runner - в качестве executor использует Shell

Регистрацию gitlab-runner определяют переменные из файла vars/runner_vars.yml. 

Примечание. Нет необходимости регистрировать именно два раннера. Всё можно было бы сделать одним раннером типа shell. Два раннера регистрируются просто для примера.

# Сервер web
Чтобы настроить сервер runner Terraform запускает плейбук main_web.yml, который объединяет плейбуки docker_all_install.yml, node_exporter.yml и cadvisor.yml.
- docker_all_install.yml устанавливает сервис Docker.
- node_exporter устанавливает и запускает Node Exporter с помощью роли prometheus.prometheus.node_exporter.
- cadvisor.yml устанавливает и запускает Cadvisor Exporter с помощью роли nahsi.cadvisor.

Таким образом, на сервере web оказываются службы Docker и службы сбора статистики и представления её в формате Prometheus. Node Exporter собирает статистику по работе самой виртуальной машины, Cadvisor собирает статистику по работе сервиса Docker. Однако само приложение установлено не будет – его собирает и устанавливает репозиторий [skillbox-service](https://gitlab.com/Dovinant/skillbox-service).
