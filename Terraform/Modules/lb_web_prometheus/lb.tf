### ---- Load Balancer ----
resource "yandex_lb_network_load_balancer" "lb" {
  #name = "lb-test"
  name = var.lb_name

  listener {
    name = var.listener_name
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.web-servers.id

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}

resource "yandex_lb_target_group" "web-servers" {
  name = var.lb_target_group

  target {
    subnet_id = data.yandex_vpc_subnet.default_b.id
    address   = yandex_compute_instance.vm-1.network_interface.0.ip_address
  }
}
