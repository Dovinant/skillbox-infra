terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}


data "yandex_compute_image" "ubuntu-20-04" {
  family = "ubuntu-2004-lts"
}

data "yandex_vpc_subnet" "default_b" {
  name = "default-ru-central1-b" # одна из наших дефолтных подсетей
}
