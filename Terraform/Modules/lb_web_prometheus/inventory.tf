#---- Generate inventory file for Ansible playbook ----
resource "local_file" "inventory" {
  filename        = "${var.ansible_working_dir}hosts_test"
  file_permission = "0666"
  content         = <<EOF
[web_servers]
web_01 ansible_host=${local.external_ip_address_web_01}

[monitoring_servers]
prometheus_01 ansible_host=${local.external_ip_address_prometheus_01}

  EOF
}
