### --- VM1 web-server ---
resource "yandex_compute_instance" "vm-1" {
  name                      = var.web_name
  allow_stopping_for_update = true
  platform_id               = "standard-v3"

  resources {
    core_fraction = 20 # Гарантированная доля vCPU
    cores         = 2
    memory        = 2
  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu-20-04.id
      type     = "network-hdd"
      size     = 10
    }
  }

  network_interface {
    subnet_id = data.yandex_vpc_subnet.default_b.id
    nat       = true
  }

  metadata = {
    serial-port-enable = 1
    user-data          = "${file("${path.module}/meta.yml")}"
  }

  scheduling_policy {
    preemptible = true # Делаем VM прерываемой
  }

  provisioner "remote-exec" {
    inline = ["echo Done!"]

    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.pvt_key)
      host        = self.network_interface.0.nat_ip_address
    }
  }

  provisioner "local-exec" {
    working_dir = var.ansible_working_dir
    command     = "ansible-playbook -i '${self.network_interface.0.nat_ip_address}', main_web.yml"
  }
}


### --- VM2 prometheus ---
resource "yandex_compute_instance" "vm-2" {
  name                      = var.prometheus_name
  allow_stopping_for_update = true
  platform_id               = "standard-v3"

  resources {
    core_fraction = 20 # Гарантированная доля vCPU
    cores         = 2
    memory        = 2
  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu-20-04.id
      type     = "network-hdd"
      size     = 15
    }
  }

  network_interface {
    subnet_id = data.yandex_vpc_subnet.default_b.id
    nat       = true
  }

  metadata = {
    serial-port-enable = 1

    user-data = "${file("${path.module}/meta.yml")}"
  }

  scheduling_policy {
    preemptible = true # Делаем VM прерываемой
  }

  provisioner "remote-exec" {
    inline = ["echo Done!"]

    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.pvt_key)
      host        = self.network_interface.0.nat_ip_address
    }
  }

  provisioner "local-exec" {
    working_dir = var.ansible_working_dir
    #command     = "ansible-playbook -i '${self.network_interface.0.nat_ip_address}', main_prometheus.yml --extra-vars prometheus_etc='./files/prometheus.test.yml.etc'"
    command     = "ansible-playbook -i '${self.network_interface.0.nat_ip_address}', main_prometheus.yml --extra-vars prometheus_etc=${var.prometheus_etc}"
  }
}
