### ---- DNS A Record ----
resource "yandex_dns_zone" "skillbox" {
  name        = var.zone_name
  description = var.zone_description

  labels = {
    label1 = var.zone_label1
  }

  zone   = var.domain_name
  public = true
}

resource "yandex_dns_recordset" "test_rs1" {
  zone_id = yandex_dns_zone.skillbox.id
  name    = var.lb_domain_name
  type    = "A"
  ttl     = 200
  data    = [local.balancer_ip_address]
}

resource "yandex_dns_recordset" "test_rs2" {
  zone_id = yandex_dns_zone.skillbox.id
  name    = var.web_domain_name
  type    = "A"
  ttl     = 200
  data    = [local.external_ip_address_web_01]
}

resource "yandex_dns_recordset" "test_rs3" {
  zone_id = yandex_dns_zone.skillbox.id
  name    = var.prometheus_domain_name
  type    = "A"
  ttl     = 200
  data    = [local.external_ip_address_prometheus_01]
}

