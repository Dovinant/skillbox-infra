### ---- Outputs ----
#output "balancer_summary" {
#  value = yandex_lb_network_load_balancer.lb-test.*
#}

output "balancer_ip_address" {
  description = "Public IP of Load Balancer instance"
  value       = [for s in yandex_lb_network_load_balancer.lb.listener : s.external_address_spec.*.address].0[0]
}

output "internal_ip_address_web-01" {
  description = "Private IP of VM1 instance"
  value       = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "external_ip_address_web-01" {
  description = "Public IP of web1 instance"
  value       = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

output "external_ip_address_prometheus_01" {
  description = "Public IP of prometheus instance"
  value       = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
}

