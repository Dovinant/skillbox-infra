#=========== main ==============
variable "cloud_id" {
  description = "The cloud ID"
  type        = string
}
variable "folder_id" {
  description = "The folder ID"
  type        = string
}
variable "default_zone" {
  description = "The default zone"
  type        = string
  default     = "ru-cenral1-a"
}
variable "pvt_key" {
  description = "SSH Private Key"
  type        = string
}
#=========== load balancer ==============
variable "lb_name" {
  description = "The name to use for all the load balancer resources"
  type        = string
}
variable "listener_name" {
  description = "The name to use for lb listener"
  type        = string
}
variable "lb_target_group" {
  description = "The name to use for lb target group"
  type        = string
}
#=========== servers ==============
variable "ssh_user" {
  description = "User for SSH connection"
  type        = string
  #type        = number
  #default = 0
}
variable "ansible_working_dir" {
  description = "Ansible working directory"
  type        = string
}
variable "web_name" {
  description = "The name to use for web server"
  type        = string
}
variable "prometheus_name" {
  description = "The name to use for prometheus server"
  type        = string
}
variable "prometheus_etc" {
  description = "Prometheus config file in etc dir"
  type = string
}
#=========== DNS ==============
variable "zone_name" {
  description = "The name to use for Yandex DNS zone"
  type        = string
}
variable "zone_description" {
  description = "The description for Yandex DNS zone"
  type        = string
}
variable "zone_label1" {
  description = "The zone label"
  type        = string
}
variable "domain_name" {
  description = "The name to use for domain"
  type        = string
}
variable "lb_domain_name" {
  description = "The domain name to use for load balancer"
  type        = string
}
variable "web_domain_name" {
  description = "The domain name to use for web server"
  type        = string
}
variable "prometheus_domain_name" {
  description = "The domain name to use for prometheus server"
  type        = string
}
### ---- Locals ----
locals {
  balancer_ip_address               = [for s in yandex_lb_network_load_balancer.lb.listener : s.external_address_spec.*.address].0[0]
  external_ip_address_web_01        = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
  external_ip_address_prometheus_01 = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
}
