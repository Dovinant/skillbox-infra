### ---- Outputs ----
#output "balancer_summary" {
#  value = yandex_lb_network_load_balancer.lb-test.*
#}

output "external_ip_address_runner" {
  description = "Public IP of runner instance"
  value       = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

output "external_ip_address_grafana" {
  description = "Public IP of grafana instance"
  value       = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
}

