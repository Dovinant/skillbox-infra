### ---- DNS A Record ----
#resource "yandex_dns_zone" "skillbox" {
#  name        = "my-prod-public-zone"
#  description = "Test public zone"

#  labels = {
#    label1 = "prod-env-public"
#  }

#  zone   = "skillbox.iz.rs."
#  public = true
#}


resource "yandex_dns_recordset" "rs3" {
  zone_id = data.yandex_dns_zone.skillbox.id
  name    = var.runner_domain_name
  type    = "A"
  ttl     = 200
  data    = [local.external_ip_address_runner_01]
}

resource "yandex_dns_recordset" "rs5" {
  zone_id = data.yandex_dns_zone.skillbox.id
  name    = var.grafana_domain_name
  type    = "A"
  ttl     = 200
  data    = [local.external_ip_address_grafana_01]
}
