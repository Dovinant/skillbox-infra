#=========== main ==============
variable "cloud_id" {
  description = "The cloud ID"
  type        = string
}
variable "folder_id" {
  description = "The folder ID"
  type        = string
}
variable "default_zone" {
  description = "The default zone"
  type        = string
  default     = "ru-cenral1-a"
}
variable "pvt_key" {
  description = "SSH Private Key"
  type        = string
}
#=========== servers ==============
variable "ssh_user" {
  description = "User for SSH connection"
  type        = string
  #type        = number
  #default = 0
}
variable "ansible_working_dir" {
  description = "Ansible working directory"
  type        = string
}
variable "runner_name" {
  description = "The name to use for web server"
  type        = string
}
variable "grafana_name" {
  description = "The name to use for prometheus server"
  type        = string
}
#=========== DNS ==============
variable "runner_domain_name" {
  description = "The domain name to use for web server"
  type        = string
}
variable "grafana_domain_name" {
  description = "The domain name to use for prometheus server"
  type        = string
}### ---- Locals ----
locals {
  external_ip_address_runner_01  = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
  external_ip_address_grafana_01 = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
}
