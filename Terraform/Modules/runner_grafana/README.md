## Файлы этого репозитория позволяют развернуть требуемую инфраструктуру в Yandex Cloud.

Модулю для работы необходимо определить значения некоторых переменных. Это сделано в файле main.tf в каталоге ../../GLOBAL.

Репозиторий содержит следующие файлы:

- main.tf - основная конфигурация Terraform
- meta.yml - файл метаданных виртуальной машины
- servers.tf - конфиргурация серверов
- dns.tf - конфигурация DNS в Yandex Cloud
- output.tf - содержит переменные в которые помещаются IP адреса серверов и балансировщика
- locals.tf - содержит переменные в которые помещаются IP адреса серверов и балансировщика. Они нужны для настройки A records DNS (см. dns.tf)
- variables.tf - описание переменных.

### main.tf
Определяет провайдера, образ для виртуальных машин (Ubuntu 20.04) и облачную сеть.

### servers.tf
Определяет виртуальные машины, всего две штуки:
- runner – GitLab runner сервер
- Grafana – система отображения статистики Grafana

Для каждой виртуальной машины определены:
- число ядер процессора и объём оперативно памяти – блок “resources”
- образ операционной системы, тип и объём виртуального диска – блок “boot_disk”
- облачная сеть – блок “network_interface”
- файл дополнительных настроек – блок “metadata”

Terraform запускает Ansible playbook для каждой виртуальной машины, однако, необходимо убедится, что VM доступна по SSH. Для этого Terraformсначала пытается установить соединение по ssh с виртуально машиной (блок provisioner “remote-exec”) и только если соединение установлено запускает соответствующий playbook (блок provisioner “local-exec”).

### dns.tf
Создаёт A RECORDS в зоне указанной переменной _var.domain_name_ (блок resource "yandex_dns_zone") для созданных VM (блоки resource "yandex_dns_recordset").
Примечание: зона уже должна быть создана, т.к. этот модуль создаёт новые записи в уже существующей зоне. Сейчас эту зону создаёт модуль lb_web_prometheus. К сожалению, Terraform не может динамически проверить существует ли ресурс и, если нет, создать его.

### output.tf
Определяет переменные, которые хранят публичные IP адреса созданных виртуальных машин и балансировщика. Показывают их после того как Terraform отработал.

### locals.tf
Определяет переменные, которые хранят публичные IP адреса созданных виртуальных машин и балансировщика. С помощью этих переменных Terraform создаёт A Records и инвентарный файл Ansible – см. файлы dns.tf и inventory.tf.

### variables.tf
Описывает переменные cloud_id, folder_id, default_zone, pvt_key. Свои значения эти переменные получают в файле terraform.tfvars. Файл terraform.tfvars в репозиторий не копируется.


## Дополнительные материалы

### Yandex Cloud
- [Создать сетевой балансировщик](https://cloud.yandex.ru/docs/network-load-balancer/operations/load-balancer-create)
- [Создать публичную зону DNS](https://cloud.yandex.ru/docs/dns/operations/zone-create-public)
- [Метаданные виртуальной машины](https://cloud.yandex.ru/docs/compute/concepts/vm-metadata)

### Terraform
- [In terraform how to skip creation of resource if the resource already exist?](https://jhooq.com/terraform-check-if-resource-exist/)
- [Terraform check if resource exists before creating it](https://stackoverflow.com/questions/70689512/terraform-check-if-resource-exists-before-creating-it)
