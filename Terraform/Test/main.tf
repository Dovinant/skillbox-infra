terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  #-------- Bucket --------
  # use terraform init -backend-config=bucket.key
  backend "s3" {
    endpoint = "storage.yandexcloud.net"
    bucket   = "bucket-for-trigger-288fe8610c01b9c3"
    region   = "ru-central1"
    key      = "my-test-web-lb.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  zone      = "ru-central1-b"
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
}

module "lb_web_prometheus" {
  source = "../Modules/lb_web_prometheus"
  #main
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  pvt_key   = var.pvt_key
  #lb
  lb_name         = "lb-test"
  listener_name   = "listener-web-servers-test"
  lb_target_group = "web-servers-test-target-group"
  #servers
  ssh_user            = "ubuntu"
  ansible_working_dir = "../../Ansible/"
  web_name            = "web01-test"
  prometheus_name     = "prometheus-test"
  prometheus_etc = "./files/prometheus.test.yml.etc"
  #DNS
  zone_name              = "my-test-public-zone"
  zone_description       = "Public zone for test environment"
  zone_label1            = "test-env-public"
  domain_name            = "test.skillbox.iz.rs."
  lb_domain_name         = "test.skillbox.iz.rs."
  web_domain_name        = "web01.test.skillbox.iz.rs."
  prometheus_domain_name = "monitoring.test.skillbox.iz.rs."
}
