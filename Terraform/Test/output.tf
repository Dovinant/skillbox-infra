### ---- Outputs ----

output "balancer_ip_address" {
  description = "Public IP of Load Balancer instance"
  value       = module.lb_web_prometheus.balancer_ip_address
}

output "internal_ip_address_web-01" {
  description = "Private IP of VM1 instance"
  value       = module.lb_web_prometheus.internal_ip_address_web-01
}

output "external_ip_address_web-01" {
  description = "Public IP of web1 instance"
  value       = module.lb_web_prometheus.external_ip_address_web-01
}

output "external_ip_address_prometheus_01" {
  description = "Public IP of prometheus instance"
  value       = module.lb_web_prometheus.external_ip_address_prometheus_01
}

