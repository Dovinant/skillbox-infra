terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  #-------- Bucket --------
  # use terraform init -backend-config=bucket.key
  backend "s3" {
    endpoint = "storage.yandexcloud.net"
    bucket   = "bucket-for-trigger-288fe8610c01b9c3"
    region   = "ru-central1"
    key      = "my-runner-grfn.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  zone      = "ru-central1-b"
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
}

module "runner_grafana" {
  source = "../Modules/runner_grafana"
  #main
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  pvt_key   = var.pvt_key
  #servers
  ssh_user            = "ubuntu"
  ansible_working_dir = "../../Ansible/"
  runner_name         = "runner"
  grafana_name        = "grafana"
  #DNS
  #zone_name              = "my-prod-public-zone"
  #zone_description       = "Public zone for prod environment"
  #zone_label1            = "prod-env-public"
  #domain_name            = "skillbox.iz.rs."
  runner_domain_name  = "runner.skillbox.iz.rs."
  grafana_domain_name = "grafana.skillbox.iz.rs."
}