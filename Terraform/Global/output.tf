### ---- Outputs ----
#output "balancer_summary" {
#  value = yandex_lb_network_load_balancer.lb-prod.*
#}

output "external_ip_address_runner_01" {
  description = "Public IP of runner instance"
  value       = module.runner_grafana.external_ip_address_runner
}

output "external_ip_address_grafana_01" {
  description = "Public IP of grafana instance"
  value       = module.runner_grafana.external_ip_address_grafana
}
