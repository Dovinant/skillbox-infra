#!/bin/bash

ansible-galaxy install -r requirements.yml
cd ./Terraform/Prod
echo "=========================================="
echo "Roll out Prod!"
echo "=========================================="
#terraform plan
terraform apply -auto-approve
cd ../Test
echo "=========================================="
echo "Roll out Test!"
echo "=========================================="
#terraform plan
terraform apply -auto-approve
cd ../Global
echo "=========================================="
echo "Roll out Runner and Grafana!"
echo "=========================================="
#terraform plan
terraform apply -auto-approve
