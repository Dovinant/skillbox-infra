#!/bin/bash

ssh-keygen -R monitoring.skillbox.iz.rs
ssh-keygen -R grafana.skillbox.iz.rs
ssh-keygen -R web1.skillbox.iz.rs
ssh-keygen -R runner.skillbox.iz.rs

cd ./Terraform/Global
echo "=========================================="
echo "Destroy out Grafana and Runner!"
echo "=========================================="
terraform destroy -auto-approve
cd ../Prod
echo "=========================================="
echo "Destroy out Prod!"
echo "=========================================="
terraform destroy -auto-approve
cd ../Test
echo "=========================================="
echo "Destroy out Test!"
echo "=========================================="
terraform destroy -auto-approve
