# Итоговый проект курса DevOps инженер от компании Skillbox

Этот репозиторий разворачивает инфраструктуры PROD, TEST и GLOBAL в Yandex Cloud.

Инфраструктура PROD содержит:
- Балансировщик нагрузки
- Веб сервер
- Сервер сбора статистики Prometheus.

Инфраструктура TEST содержит:
- Балансировщик нагрузки
- Веб сервер
- Сервер сбора статистики Prometheus

Инфрастуктура GLOBAL
- Runner сервер для Gitlab
- Сервер отображения данных Grafana.


Обоснование выбора системы мониторинга описано в файле LADR.txt

Сервис на готовой инфраструктуре разворачивает и запускает другой [репозиторий](https://gitlab.com/Dovinant/skillbox-service).

## Предварительные замечания

Все сервера работают под управлением Ubuntu 20.04. Установка проводилась с рабочих станций под управлением Ubuntu 20.04 и Linux Mint 20.3 (Una).
Anisible на управляющей рабочей станции должен быть установлен через pip, не через apt! Если ansible был установлен через apt, его следует удалить командой
> `sudo apt remove ansible`
и установить
> `python3 -m pip install --user ansible`

Файл состояния Terraform хранится в Yandex s3 backend. Инициализацию Terraform следует выполнять командой:
> `terraform init -backend-config=bucket.key`

Где bucket.key содержит access_key secret_key для доступа к S3. Файла bucket.key нет в репозитории, его надо создавать отдельно. [Подробнее](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-state-storage#set-up-backend)

## Быстрый старт
[Установите Terraform и настройте его для работы с Yandex Cloud](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart#install-terraform).

Клонируйте репозиторий.
В каталоге Terraform создайте файл terraform.tfvars и определите в нём переменные:
- cloud_id  = "идентификатор вашего облака"
- folder_id = "идентификатор каталога в облаке"
- pvt_key   = "путь к приватному ssh ключу"

Из корневого каталога дайте команду ./install.sh. Этот скрипт установит необходимые роли и коллекции ansible, развернёт и настроит сервера web, monitoring, grafana, gitlab-runner и network load balancer в Yandex Cloud. Если скрипт отработает успешно, будут развёрнуты два окружения и в них станут доступны следующие сервера:

Окружение PROD:
- [http://skillbox.iz.rs](http://skillbox.iz.rs)
- [http://web01.skillbox.iz.rs](http://web01.skillbox.iz.rs)
- [http://monitoring.skillbox.iz.rs:9090](http://monitoring.skillbox.iz.rs:9090)

Окружение TEST:
- [http://test.skillbox.iz.rs](http://test.skillbox.iz.rs)
- [http://web01.test.skillbox.iz.rs](http://web01.test.skillbox.iz.rs)
- [http://monitoring.test.skillbox.iz.rs:9090](http://monitoring.test.skillbox.iz.rs:9090)

Окружение GLOBAL:
- [http://grafana.skillbox.iz.rs:3000](http://grafana.skillbox.iz.rs:3000)
- runner.skillbox.iz.rs

Кроме того, в gitlab.com для репозитория [skillbox-diploma](https://gitlab.com/Dovinant/skillbox-diploma) зарегистрируются два runners:

- Example Shell GitLab Runner - в качестве executor использует Shell
- Example Docker GitLab Runner - в качестве executor использует Docker

## Балансировщик skillbox.iz.rs (и test.skillbox.iz.rs)
Это сетевой балансировщик Yandex Cloud. Он распределяет нагрузку по группе web серверов. Сейчас группа состоит только из одного сервера web01.skillbox.iz.rs.

## Сервер web01.skillbox.iz.rs (и web01.test.skillbox.iz.rs)
Это web сервер на котором работает наш сервис.
Внимание! Данный репозиторий разворачивает только инфраструктуру. После успешного выполнения скрипта install.sh, этот сервер не будет доступен по 80-му порту. Сервис будет развёрнут в результате работы pipline репозитория [skillbox-diploma](https://gitlab.com/Dovinant/skillbox-diploma). Само развёртывание обеспечивает репозиторий [skillbox-service](https://gitlab.com/Dovinant/skillbox-service)

## Сервер monitoring.skillbox.iz.rs:9090 (и monitoring.test.skillbox.iz.rs)
На этом сервере развёрнут Prometheus. Он собирает статистику с нескольких exporters:
- Node-exporter – отправляет статистику работы линукс сервера. Установлен на web01.
- Cadvisor – отправляет статистику работы Docker. Установлен на web01.
- Blackbox – отправляет статистику по HTML запросам. Мониторит балансировщик, установлен на web01.
- Yandex Network Load Balancer также отправляет статистику в формате Prometheus (experimental).

## Сервер grafana.skillbox.iz.rs
Отображает собранную статистику в графическом виде. Несколько панелей выполняют эту работу:
- Docker_monitoring_with_service_selection – работа Docker
- Node_Exporter_Full – работа линукс сервера. Полная статистика
- node_exporter_basics – работа линукс сервера. Сокращённая статистика
- Prometheus_Blackbox_Exporter – работа HTTP
- Go_Metrics – работа Go. Эти метрики отсылает само наше приложение

Сервер Grafana позволяет собирать статистику с окружения prod и dev. Для этого в Grafana зарегистрированы два источника данных - один для окружения prod, другой для dev (сейчас они оба указывает на один и тот же сервер Prometheus, т.к. сейчас у нас только одно окружение). В каждой Dashboard можно выбрать с какого источника отображать информацию.

## Структура репозитория
Репозиторий содержит файлы:
- README.md (этот файл)
- install.sh – скрипт устанавливает необходимые роли и коллекции Ansible и создаёт инфраструктуру с помощью Terraform
- uninstall.sh – удаляет созданную инфраструктуру и удаляет ssh ключи для серверов web01, monitoring, grafana
- requirements.yml - содержит необходимые роли и коллекции Anisible.

Репозиторий содержит каталоги Terraform и Ansible. Описание файлов и их назначений смотрите в README этих каталогов.

## Как удалить инфраструктуры
Из корневого каталога дайте команду ./uninstall.sh. Этот скрипт удалить обе инфраструктуры и удалит записи ssh ключей серверов.

## Дополнительные материалы

### Yandex Cloud
- [Начало работы с Terraform](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart)
- [Как начать работать с Cloud DNS](https://cloud.yandex.ru/docs/dns/quickstart)

### DNS
- [www.iz.rs - Бесплатная регистрация доменов третьего уровня](https://www.iz.rs/postupak_registracije)
- [FreeDNS: No-Charge Domain Hosting](https://freedns.afraid.org/)
